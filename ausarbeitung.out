\BOOKMARK [0][-]{chapter.1}{Einleitung}{}% 1
\BOOKMARK [1][-]{section.1.1}{Motivation}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{Zielsetzung}{chapter.1}% 3
\BOOKMARK [0][-]{chapter.2}{System\374bersicht und Definitionen}{}% 4
\BOOKMARK [1][-]{section.2.1}{System\374bersicht}{chapter.2}% 5
\BOOKMARK [1][-]{section.2.2}{Digitale Bilder}{chapter.2}% 6
\BOOKMARK [0][-]{chapter.3}{Lokalisierung von Text}{}% 7
\BOOKMARK [1][-]{section.3.1}{Connected Components}{chapter.3}% 8
\BOOKMARK [2][-]{subsection.3.1.1}{Nachbarschaft von Pixeln \(Pixel Connectivity\)}{section.3.1}% 9
\BOOKMARK [2][-]{subsection.3.1.2}{Definition einer Connected Component}{section.3.1}% 10
\BOOKMARK [2][-]{subsection.3.1.3}{Attribute der Connected Components}{section.3.1}% 11
\BOOKMARK [1][-]{section.3.2}{Maximally Stable Extremal Regions}{chapter.3}% 12
\BOOKMARK [2][-]{subsection.3.2.1}{Bin\344res Thresholding}{section.3.2}% 13
\BOOKMARK [2][-]{subsection.3.2.2}{Definitionen}{section.3.2}% 14
\BOOKMARK [2][-]{subsection.3.2.3}{Implementierung}{section.3.2}% 15
\BOOKMARK [1][-]{section.3.3}{Gradienten}{chapter.3}% 16
\BOOKMARK [2][-]{subsection.3.3.1}{Intuition}{section.3.3}% 17
\BOOKMARK [2][-]{subsection.3.3.2}{Definition}{section.3.3}% 18
\BOOKMARK [2][-]{subsection.3.3.3}{Berechnung des Gradienten}{section.3.3}% 19
\BOOKMARK [1][-]{section.3.4}{Canny Edge Detection}{chapter.3}% 20
\BOOKMARK [2][-]{subsection.3.4.1}{Vorbearbeitung}{section.3.4}% 21
\BOOKMARK [2][-]{subsection.3.4.2}{Lokalisierung der Kanten}{section.3.4}% 22
\BOOKMARK [2][-]{subsection.3.4.3}{Hysteresis Thresholding}{section.3.4}% 23
\BOOKMARK [1][-]{section.3.5}{Edge Enhanced MSER}{chapter.3}% 24
\BOOKMARK [2][-]{subsection.3.5.1}{Durchf\374hrung des Edge Enhancing}{section.3.5}% 25
\BOOKMARK [1][-]{section.3.6}{Stroke Width Transform}{chapter.3}% 26
\BOOKMARK [2][-]{subsection.3.6.1}{Distance Transform}{section.3.6}% 27
\BOOKMARK [2][-]{subsection.3.6.2}{Anwendung der Stroke Width Transform}{section.3.6}% 28
\BOOKMARK [1][-]{section.3.7}{Robuste Textlokalisierung}{chapter.3}% 29
\BOOKMARK [0][-]{chapter.4}{Erkennung von Text}{}% 30
\BOOKMARK [1][-]{section.4.1}{Neuronale Netze}{chapter.4}% 31
\BOOKMARK [2][-]{subsection.4.1.1}{Neuronen}{section.4.1}% 32
\BOOKMARK [2][-]{subsection.4.1.2}{Bildung von neuronalen Netzwerken}{section.4.1}% 33
\BOOKMARK [2][-]{subsection.4.1.3}{Aktivierungsfunktionen}{section.4.1}% 34
\BOOKMARK [2][-]{subsection.4.1.4}{Trainingsdaten}{section.4.1}% 35
\BOOKMARK [2][-]{subsection.4.1.5}{Kostenfunktion}{section.4.1}% 36
\BOOKMARK [2][-]{subsection.4.1.6}{Gradientenverfahren}{section.4.1}% 37
\BOOKMARK [2][-]{subsection.4.1.7}{Stochastisches Gradientenverfahren}{section.4.1}% 38
\BOOKMARK [1][-]{section.4.2}{Convolutional Neuronal Networks}{chapter.4}% 39
\BOOKMARK [2][-]{subsection.4.2.1}{Tensoren}{section.4.2}% 40
\BOOKMARK [2][-]{subsection.4.2.2}{Schichten}{section.4.2}% 41
\BOOKMARK [1][-]{section.4.3}{Synthetische Trainingsdaten}{chapter.4}% 42
\BOOKMARK [1][-]{section.4.4}{Entwickeltes CNN}{chapter.4}% 43
\BOOKMARK [2][-]{subsection.4.4.1}{Architektur}{section.4.4}% 44
\BOOKMARK [2][-]{subsection.4.4.2}{Training}{section.4.4}% 45
\BOOKMARK [0][-]{chapter.5}{Merging und Fehlerkorrektur}{}% 46
\BOOKMARK [1][-]{section.5.1}{Merging}{chapter.5}% 47
\BOOKMARK [1][-]{section.5.2}{Fehlerkorrektur}{chapter.5}% 48
\BOOKMARK [2][-]{subsection.5.2.1}{Levenshtein Distanz}{section.5.2}% 49
\BOOKMARK [2][-]{subsection.5.2.2}{Korrektur}{section.5.2}% 50
\BOOKMARK [2][-]{subsection.5.2.3}{Beispiel}{section.5.2}% 51
\BOOKMARK [0][-]{chapter.6}{Evaluation}{}% 52
\BOOKMARK [1][-]{section.6.1}{Textlokalisierung}{chapter.6}% 53
\BOOKMARK [2][-]{subsection.6.1.1}{Precision und Recall}{section.6.1}% 54
\BOOKMARK [2][-]{subsection.6.1.2}{Vergleich}{section.6.1}% 55
\BOOKMARK [1][-]{section.6.2}{Texterkennung}{chapter.6}% 56
\BOOKMARK [2][-]{subsection.6.2.1}{Confusion Matrix}{section.6.2}% 57
\BOOKMARK [1][-]{section.6.3}{Komplettes System}{chapter.6}% 58
\BOOKMARK [1][-]{section.6.4}{Laufzeit}{chapter.6}% 59
\BOOKMARK [0][-]{chapter.7}{Schlussbemerkung und Ausblick}{}% 60
\BOOKMARK [1][-]{section.7.1}{Ausblick}{chapter.7}% 61
\BOOKMARK [0][-]{chapter.8}{Anhang}{}% 62
\BOOKMARK [1][-]{section.8.1}{Stroke Width Transform Strichbreite}{chapter.8}% 63
