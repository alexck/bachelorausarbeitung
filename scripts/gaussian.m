sigma = 1;
tx = ty = linspace(-3, 3, 50);
dx = dy = [-3, -2, -1, 0, 1, 2, 3]
[xx, yy] = meshgrid(tx, ty);
[dxx, dyy] = meshgrid(dx, dy);
tz = (1/(2*pi*sigma.^2)) * exp(-(xx.^2+yy.^2)/(2*sigma.^2));
dz = (1/(2*pi*sigma.^2)) * exp(-(dxx.^2+dyy.^2)/(2*sigma.^2));
mesh(tx, ty, tz);
hold
stem3(dxx, dyy, dz, "r")
xlabel("x")
ylabel("y")
zlabel("z")
print -dsvg foo.svg