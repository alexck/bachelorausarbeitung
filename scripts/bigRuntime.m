xBig = [1,2,3,4,5,6,7,8,9,10,11,12,13]
yBig = [2.87683,2.70415,2.77605,2.73538,2.76671,2.81438,2.85098,2.70451,2.7168,2.80427,2.8086,2.94899,2.79541]
zBig = [1.48838,0.70577,0.599489,0.60472,0.970607,1.01915,1.16978,0.735191,0.496721,0.508913,0.761003,1.16111,0.597917]

xMedium = [1,2,3,4,5,6,7]
yMedium = [2.08412,1.95757,2.00978,1.86672,2.06714,1.88196,2.12014]
zMedium = [1.62181,0.614071,0.783197,0.571474,0.799972,0.536905,0.892517]

xSmall = [1,2,3,4,5,6,7,8]
ySmall = [1.06434,1.19395,1.16473,1.16643,1.08914,1.03573,1.05567,1.04574]
zSmall = [1.19085,0.527694,0.497343,0.533562,0.583914,0.418643,0.417906,0.55916]

aBig = yBig + zBig
aMedium = yMedium + zMedium
aSmall = ySmall + zSmall

meanBig = mean(aBig)
medianBig = median(aBig)
meanBigDetect = mean(yBig)
medianBigDetect = median(yBig)
meanBigRecog = mean(zBig)
medianBigRecog = median(zBig)

meanMedium = mean(aMedium)
medianMedium = median(aMedium)
meanMediumDetect = mean(yMedium)
medianMediumDetect = median(yMedium)
meanMediumRecog = mean(zMedium)
medianMediumRecog = median(zMedium)

meanSmall = mean(aSmall)
medianSmall = median(aSmall)
meanSmallDetect = mean(ySmall)
medianSmallDetect = median(ySmall)
meanSmallRecog = mean(zSmall)
medianSmallRecog = median(zSmall)
#stem(xSmall,aSmall)
#set (gca, "ygrid", "on");
#xlabel("Bild Nummer")
#ylabel("Laufzeit (Sekunden)")
#print -depsc big.eps