\chapter{Merging und Fehlerkorrektur}
\label{chp:merge}
\begin{figure}[h!]
  \centering
  \subfloat[Lokalisierte und erkannte Zeichen]{\includegraphics[width=0.32\textwidth]{merge/detected}}
  \hfill
	\subfloat[Horizontale Erweiterung der Bounding Boxen]{\includegraphics[width=0.32\textwidth]{merge/extended}}
  \hfill
  \subfloat[Merging und Fehlerkorrektur]{\includegraphics[width=0.3\textwidth]{merge/merged}}
  \caption{Wortbildung durch Merging sowie Korrektur von Fehlern}
  \label{fig:merge}
\end{figure}

Mithilfe der Textlokalisierung werden in einem digitalen Bild Textzeichen durch
eine Bounding Box markiert. Der Inhalt dieser wird der Texterkennung übergeben,
die feststellt, welches Zeichen erkennbar ist. Abbildung ~\ref{fig:merge} zeigt
links mehrere Rechtecke und die dazugehörigen erkannten Textzeichen. In diesem
Kapitel wird zuerst eine Methode vorgestellt, die mithilfe der beschrifteten
Rechtecke Wörter bildet. Dabei wird ähnlich wie in \cite{mattext} vorgegangen.
Anschließend werden die Wörter einer Fehlerkorrektur übergeben, welche ebenfalls
in diesem Kapitel beschrieben wird.

\section{Merging}
\label{sec:merge}
Es sei $R$ die Menge der beschrifteten Rechtecke. In dieser befinden sich Tupel
$(x, y, w, h, s)$. $x,y,w,h$ werden wie in Abschnitt ~\ref{sec:ccatt} zur
Darstellung der Bounding Box verwendet und $s$ gibt das von der Texterkennung
erkannte Zeichen an. Wie im mittigen Bild der Abbildung ~\ref{fig:merge}
erkennbar ist, werden alle Rechtecke der Menge $R$ horizontal beidseitig um $n$
Pixel erweitert. Da der in Pixeln gemessene Abstand zwischen zwei Zeichen je
nach Schriftgröße kleiner oder größer ist, wird $n$ gemäß der Breite des
Rechtecks angepasst. Dazu wird eine Funktion $f: \realnumbers^{+} \rightarrow
\realnumbers^{+}$ definiert, wobei $f(w)=(1+\alpha)w-w$ gilt. Der Parameter
$\alpha \in \realnumbers^{+}$ gibt an, wie viel horizontal erweitert wird. In
der Implementierung wurde $\alpha=0.33$ gewählt. Formal lässt sich die
Erweiterung der Rechtecke als Erzeugung einer neuen Menge $R_{e}$ beschreiben,
wobei gilt:
\begin{equation}
  R_{e}=\{(x-f(w),y,w+2f(w),h,s) \mid (x,y,w,h,s)\in R\}
\end{equation}

Als Nächstes werden alle überlappenden Rechtecke der Menge $R_{e}$ paarweise
zusammengefasst. Dazu wird ein beliebiges Element
$r=(x_{1},y_{1},w_{1},h_{1},s_{1})$ der Menge $R_{e}$ gewählt. Ausgehend von $r$
werden rekursiv alle mit $r$ verbundene, beschriftete Rechtecke ermittelt und in
einer Menge $R_{n} \subseteq R_{e}$ gespeichert. Beispielsweise sei $r$ das
Rechteck mit Zeichen \textit{G} in Abbildung ~\ref{fig:merge}. Da $r$ sich mit
dem Rechteck von \textit{a} überlappt, wird dieses zu $R_{n}$ hinzugefügt. Damit
wird auch das Rechteck von \textit{z} hinzugefügt, da sich dieses mit dem des
Zeichens \textit{a} überlappt und so weiter. Eine Überlappung mit sich selbst
ist nicht gültig. Anschließend wird die in Algorithmus ~\ref{alg:merge}
beschriebene Methode (welche Algorithmus ~\ref{alg:mergetwo} aufruft) zum Zusammenfassen von $r$ und $R_{n}$ verwendet. So
entsteht ein neues beschriftetes Rechteck das in einer Menge $R_{o}$ gespeichert
wird. Darauf folgend werden $r$ und alle Elemente von $R_{n}$ aus $R_{e}$
entfernt und von vorne begonnen, also wieder ein Element der Menge $R_{e}$
gewählt. Dies wird so lange wiederholt, bis sich in $R_{e}$ keine Elemente mehr
befinden. Abschließend wird die Menge $R_{o}$ der im nächsten Abschnitt
beschriebenen Fehlerkorrektur übergeben.

\begin{algorithm}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \textcolor{cadmiumgreen}{\textbf{Merge}}$(r,R_{n})$\;
    \Indp\Input{$r$: ein beschriftetes Rechteck mit $r=(x_{1},y_{1},w_{1},h_{1},s_{1})$, $R_{n}$: eine Menge von Rechtecken, die sich mit $r$ überlappen}
    \Output{Das zusammengefasste beschriftete Rechteck}
		\BlankLine
    \ForEach{$r_{n} \in R_{n}$}
    {
      $r \gets \text{MergeTwoRectangles}(r, r_{n})$
    }
    \textbf{return} $r$\;
    \caption{Methode zum Zusammenfassen von überlappenden Rechtecken}
		\label{alg:merge}
\end{algorithm}

\begin{algorithm}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \textcolor{cadmiumgreen}{\textbf{MergeTwoRectangles}}$(r_{1},r_{2})$\;
    \Indp\Input{$r_{1},r_{2}$: zwei beschriftete Rechtecke mit $r_{1}=(x_{1},y_{1},w_{1},h_{1},s_{1})$ und $r_{2}=(x_{2},y_{2},w_{2},h_{2},s_{2})$}
    \Output{Das zusammengefasste beschriftete Rechteck}
		\BlankLine
		$x \gets \text{min}(x_{1},x_{2})$\;
    $y \gets \text{min}(y_{1},y_{2})$\;
    $w \gets \text{max}(x_{1} + w_{1}, x_{2} + w_{2}) - x$\;
    $h \gets \text{max}(y_{1} + h_{1}, y_{2} + h_{2}) - y$\;
    \BlankLine
    \If{$x_{1} \le x_{2}$}
    {
      $s \gets s_{1}||s_{2}$\Comment*{$s_{1}||s_{2}$ ist Konkatenation der Zeichenketten $s_{1}$ und $s_{2}$}
    }
    \Else
    {
      $s \gets s_{2}||s_{1}$\;
    }
    \textbf{return} $(x,y,w,h,s)$\;
    \caption{Methode zum Zusammenfassen von zwei beschrifteten Rechtecken}
		\label{alg:mergetwo}
\end{algorithm}


\section{Fehlerkorrektur}
\label{sec:correct}
Wie anhand der gelben Boxen in Abbildung ~\ref{fig:merge} erkennbar ist, treten
bei der Texterkennung Fehler auf. Die Groß- und Kleinschreibung einiger Zeichen
wird verwechselt. Ebenfalls schlägt die Unterscheidung von ähnlichen Zeichen wie
''0'' und ''O'' fehl. In diesem Abschnitt wird eine Methode zur Korrektur
von Fehlern, die bei der Texterkennung entstanden sind, beschrieben.

Die grundlegende Idee ist, ein Wörterbuch und die in \cite{levenshtein1966binary}
vorgestellte Levenshtein Distanz zu verwenden.

\subsection{Levenshtein Distanz}
Die Levenshtein Distanz $\text{lev}(u,v)$ gibt an, wie ähnlich sich zwei
Zeichenketten $u$ und $v$ sind. Genauer gesagt ist sie die minimale Anzahl von
Operationen, die durchgeführt werden müssen, damit $u=v$ gilt. Dabei sind nach
\cite[7]{navarro2001guided} folgende Operationen erlaubt:

\begin{enumerate}
  \item Einfügen eines beliebigen Zeichens in $u$.
  \item Löschen eines beliebigen Zeichens aus $u$.
  \item Ersetzen eines Zeichens von $u$ durch ein beliebiges anderes.
\end{enumerate}

Es gilt $\text{lev}(u,v)=\text{lev}(v,u)$ \cite[7-8]{navarro2001guided}.

Zur Verdeutlichung der Levenshtein Distanz sei beispielsweise $u=\text{Terasse}$
und $v=\text{Tasse}$. Ein Weg um $u=v$ zu erreichen ist das Löschen des 2. und
3. Zeichen von $u$. Da es nicht möglich ist $u=v$ mit weniger als zwei
Operationen zu erreichen, gilt $lev(u,v)=2$.

\subsection{Korrektur}
Da bereits bekannt ist, welche Wörter $w$ sich in den Bildern von Tankschächten
befinden, werden diese in ein Wörterbuch $W$ eingetragen. Zusätzlich wird zu
jedem Wort eine maximale Distanz $d \in \naturalnumbers_{0}$ gespeichert. Formal
ist $W$ eine Menge von Paaren $(w,d)$.

Nacheinander werden alle $(x, y, w, h, s) \in R_{o}$ korrigiert, wobei $R_{o}$
der Output der im vorherigen Abschnitt beschriebenen Merging Methode ist. Dazu
werden mögliche Korrekturen ermittelt, indem nur jene $(w,d)\in W$ untersucht
werden, für die $\text{lev}(w, s) \le d$ gilt. Es ergeben sich folgende Fälle:

\begin{enumerate}
  \item Kein $(w,d)\in W$ erfüllt die Bedingung $\text{lev}(w, s) \le d$. Dann ist
$s$ keinem Wort des Wörterbuchs ähnlich genug, um korrigiert zu werden. $s$
bleibt unverändert.
  \item Genau ein $(w,d)\in W$ erfüllt das Kriterium. In diesem Fall wird $s$
durch $w$ ersetzt.
  \item $(w_{1},d_{1}),...,(w_{n},d_{n})$ werden der Voraussetzung gerecht. Dann
wird dasjenige Paar $(w_{i},d_{i})$ gewählt, das die geringste Levenshtein
Distanz $\text{lev}(w_{i},s)$ hat. Anschließend wird $s$ durch $w_{i}$ ersetzt.
  \item Wenn in $(3)$ kein Minimum bestimmt werden kann, ist eine eindeutige
Korrektur nicht möglich. $s$ bleibt unverändert.
\end{enumerate}

\subsection{Beispiel}
Folgendes Beispiel verdeutlicht die Vorgehensweise. Das Wörterbuch $W$ sei
gegeben als $W=\{(Benzin,2)$,\allowbreak$(Diesel,2)$,$(Plus,1)\}$. Bei den
Wörtern Benzin und Diesel dürfen somit 2 Fehler auftreten, bei Plus einer. Das
durch Merging entstandene Wort sei $s=enzin$. Es ergeben sich folgende
Levenshtein Distanzen. $\text{lev}(Benzin,enzin)=1$,
$\text{lev}(Diesel,enzin)=6$ und $\text{lev}(Plus,enzin)=5$. Die Fehlerkorrektur
würde in diesem Fall nach den oben genannten Regeln $s=enzin$ mit dem Wort
Benzin ersetzen.
