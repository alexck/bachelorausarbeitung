\chapter{Erkennung von Text}
\label{chp:textrecognition}
Ziel dieses Kapitel ist ein Verfahren vorzustellen, das als Eingabe ein
Graustufenbild eines einzelnen Textzeichens erhält und feststellt, welches von 62
möglichen Zeichen $c_{i}$ sich auf dem Bild befindet. Dabei gilt
$c=(A,...,Z$,\allowbreak$a,...,z$,$0,...,9)$. Erkannt werden also Groß- und
Kleinbuchstaben des lateinischen Alphabet sowie die Ziffern $0$ bis $9$.
Verwendet wird ein Convolutional Neural Network (CNN), das mit synthetischen
Trainingsdaten trainiert wurde. Die Vorgehensweise ähnelt der in
\cite{jaderberg2016reading} beschriebenen Methode mit dem Unterschied, dass in
dieser Arbeit einzelne Zeichen und nicht komplette Wörter vom CNN erkannt
werden.

Vorerst werden in diesem Kapitel Neuronale Netze sowie die auf ihnen basierenden
Convolutional Neuronal Networks allgemein beschrieben. Da zur Erzeugung eines
robusten CNN neben dem Aufbau des Netzwerks die Wahl der Trainingsdaten relevant
ist, werden diese in Sektion ~\ref{sec:train} detailliert beschrieben.
Abschließend wird in Abschnitt ~\ref{sec:devcnn} das für diese Arbeit
entwickelte CNN vorgestellt.

\section{Neuronale Netze}
Neuronale Netze sowie CNNs sind Methoden des Maschinellen Lernens.  Beide
Systeme ''lernen'' mithilfe von Trainingsdaten selbstständig ein Problem zu
lösen. Die grundlegende Idee im Anwendungsfall der Texterkennung lässt sich wie
folgt zusammenfassen. Das System erhält Paare von Bildern und Labels. Die Labels
geben an, welches Textzeichen sich auf dem zugehörigen Bild befindet. Durch ein
Optimierungsverfahren werden die Parameter des Systems so angepasst, dass der
Output des Systems für ein Eingabebild dem Label gleicht. Nachdem der Lernvorgang
abgeschlossen ist, wird mithilfe anderer Bilder, die nicht Teil der Trainingsdaten
sind, überprüft, ob eine allgemeine Lösung gefunden wurde. Die folgende Beschreibung von neuronalen Netzen basiert auf \cite{nielsenneuronal}.

\subsection{Neuronen}
\begin{figure}[t!]
	\centerfloat
  \includegraphics[width=0.7\textwidth]{recognize/nn/neuron.pdf}
	\caption{Einzelnes Neuron}
	\label{fig:neuron}
\end{figure}
Neuronale Netze bestehen aus mehreren Neuronen. Diese erhalten Eingaben
$x_{1}$,$...$,\allowbreak$x_{n}\in \realnumbers$, führen eine Berechnung durch und erzeugen so
einen Output $a$. Der Output wird häufig auch als Aktivierung bezeichnet. Die
Eingaben sind mit Gewichtungen $w_{1},...,w_{n}\in \realnumbers$ versehen, wobei
jedes $w_{i}$ angibt, wie viel Einfluss eine Eingabe $x_{i}$ auf den Output hat.
Zusätzlich verfügt jedes Neuron über einen Bias $b$, der ebenfalls in die
Berechnung einbezogen wird. Der Output $a$ lässt sich wie folgt bestimmen.
$a=f(\sum_{i=1}^{n}{w_{i}x_{i}} + b)$. Eine Vereinfachung kann erreicht werden,
indem die Eingaben und Gewichtungen als Vektoren aufgefasst werden, also
$x=(x_{1},...,x_{n})$ und $w=(w_{1},...,w_{n})$. Dann gilt, $a=x\bigcdot w + b$
wobei $x\bigcdot w$ das Skalarprodukt von $x$ und $w$ ist. $f$ steht für die
Aktivierungsfunktion des Neurons die in Abschnitt ~\ref{sec:activation} genauer beschrieben wird. Abbildung ~\ref{fig:neuron} visualisiert das
Beschriebene. Die Eingaben und Gewichtungen befinden sich links im Bild, die Ausgabe rechts.
\subsection{Bildung von neuronalen Netzwerken}
\begin{figure}[t!]
	\centerfloat
  \includegraphics[width=0.75\textwidth]{recognize/nn/net.pdf}
	\caption{Neuronales Netzwerk}
	\label{fig:nn}
\end{figure}
Zur Bildung eines neuronalen Netzwerks werden Neuronen schichtenweise angeordnet,
wie in Abbildung ~\ref{fig:nn} erkennbar ist. Jedes Neuron der ersten Schicht
erhält als Eingabe einen Vektor $x$. Die Neuronen der nachfolgenden Schichten
erhalten als Eingabe die Ausgabe von allen Neuronen der vorherigen Schicht. Zur
formellen Beschreibung von neuronalen Netzen wird die in \cite{nielsenneuronal}
vorgestellte Notation genutzt.

\begin{enumerate}
	\item $b_{j}^{l}$ gibt den Bias des $j$-ten Neurons
	in der $l$-ten Schicht an.
	\item $w_{jk}^{l}$ steht für die Gewichtung vom $k$-ten
	Neuron der $(l-1)$-ten Schicht zum $j$-ten Neuron der $l$-ten Schicht.
	\item $a_{j}^{l}$ gibt den Output des $j$-ten Neurons der $l$-ten Schicht an.
	\item Die Gewichtungsmatrix $w^{l}$ der $l$-ten Schicht speichert im Eintrag der $j$-ten Reihe und $k$-ten Spalte die Gewichtung $w_{jk}^{l}$.
	\item Der Biasvektor $b^{l}$ der $l$-ten Schicht speichert im $j$-ten Eintrag den Bias $b_{j}^{l}$.
\end{enumerate}

Mithilfe dieser Definitionen lässt
sich die gewichtete Eingabe $z^{l}$ für die $l$-te Schicht berechnen als:
\begin{equation}\label{eq:nn}
	z^{l}=w^{l}a^{l-1} + b^{l}
\end{equation}

Der Output Vektor $a^{l}$ der $l$-ten Schicht ist gegeben als:
\begin{equation}
	a^{l}=f(z^{l})
\end{equation}

Es sei $L$ die Anzahl von Schichten aus denen das neuronale Netz besteht. In
Abbildung ~\ref{fig:nn} gilt beispielsweise $L=3$. Der Output des gesamten Netzes
lässt sich berechen als:
\begin{equation}
	a^{L}=f(z^{L})
\end{equation}

Wie in Gleichung \eqref{eq:nn} erkennbar ist, basiert die gewichtete Eingabe
$z^{l}$ der $l$-ten Schicht und somit der Output $a^{l}$ auf dem Output
$a^{l-1}$ der $(l-1)$-ten Schicht. Da die $1$-te Schicht keinen Vorgänger hat, wird festgelegt dass $a^{0}$ der Input $x$ des neuronalen Netzes ist.

\subsection{Aktivierungsfunktionen}
\label{sec:activation}
In der Praxis werden für die bisher nicht genauer beschriebene
Aktivierungsfunktion $f$ unter anderem folgende Funktionen verwendet.

Sigmoid Funktion:
\begin{equation}
	\sigma(x)=\frac{1}{(1+e^{-x})}
\end{equation}
Tanh:
\begin{equation}
	\text{tanh}(x)=2\sigma(2x)-1
\end{equation}
ReLU \cite{krizhevsky2012imagenet}:
\begin{equation}
	f(x)=\text{max}(0,x)
\end{equation}

In \cite{cs231n} wird die Verwendung von ReLU empfohlen. Aus diesem Grund wird im weiteren Verlauf dieses Kapitels, wenn nicht anders angegeben, ReLU verwendet.

Wie erkennbar ist, sind sowohl Eingabe als auch Ausgabe der
Aktivierungsfunktionen Skalare. Im vorherigen Abschnitt wurde die Gleichung
$a^{l}=f(z^{l})$ angegeben, bei der sowohl $a^{l}$ als auch $z^{l}$ Vektoren
sind. Dies ist eine Kurzschreibweise für die formal korrektere Gleichung
$a^{l}_{j}=f(z^{l}_{j})$

\subsection{Trainingsdaten}
Die Trainingsdaten
$T=\{(x^{(0)},y^{(0)}),...$,\allowbreak$(x^{(n)},y^{(n)})\}$ seien Paare $(x,y)$
wobei $x\in \realnumbers^{k}$ der Eingabevektor des neuronalen Netzes sei und $y\in \realnumbers^{m}$ das Label von $x$ ist. Dabei gilt $k,m\in \naturalnumbers$.

Im Kontext der Texterkennung ist zu beachten, dass in den Labels gespeichert werden muss, welches Zeichen $c_{i}$ mit $c=(A,...,Z$,\allowbreak$a,...,z$,$0,...,9)$ sich auf dem zugehörigen Bild $x$ befindet. Um diese Information in $y$ zu speichern, wird die One-Hot-Kodierung verwendet. Dazu wird festgelegt, dass $y\in \realnumbers^{62}$ gilt und ein Eintrag $y_{i}$ von $y$ genau dann eins ist, wenn sich im dazugehörigen Bild $x$ das Zeichen $c_{i}$ mit Index $i$ befindet. Alle anderen Einträge von $y$ erhalten den Wert null.

Beispielsweise sei im Bild $x$ das Zeichen \textit{B} erkennbar. Dann ist das zugehörige Label $y=(0,1,\underbrace{...}_\text{59 mal 0},0)$. Für das Zeichen \textit{C} wäre $y=(0,0,1,\underbrace{...}_\text{58 mal 0},0)$

\label{sec:costfunction}
\subsection{Kostenfunktion}
Neben den Trainingsdaten ist eine Kostenfunktion $C$ notwendig, die gegen $0$
geht, falls für alle $(x,y) \in T$ der Output $a^{L}$ des Netzwerks bei Eingabe
$x$ gleich $y$ ist. In der Implementierung wurde für $C$ die Kreuzentropie
verwendet. Diese ist für Labels mit $m$ Komponenten gegeben als:
\begin{equation}
	C=-\frac{1}{|T|}\sum_{(x,y)\in T}{\sum_{i=1}^{m}{(y_{i}\text{ ln}(a_{i}^{L})+(1-y_{i})\text{ ln}(1-a_{i}^{L})))}}
\end{equation}
Der Vorteil der Kreuzentropie gegenüber Alternativen wie der mittleren quadratischen Abweichung ist, dass das neuronale Netz unter Verwendung dieser Kostenfunktion in der Regel schneller lernt.

Zu beachten ist, dass die Kreuzentropie zwei Wahrscheinlichkeitsmaße (W-Maße) vergleicht. Um sicherzustellen, dass der Output $a^{L}$ des Netzwerks ein W-Maß ist, wird die Softmax Funktion als Aktivierungsfunktion der Neuronen der letzten Schicht des Netzwerks verwendet. Dann ist der Output des $j$-ten Neurons der $L$-ten Schicht mit gewichteter Eingabe $z_{j}^{L}$ gegeben als:

\begin{equation}
	a_{j}^{L}=\frac{e^{z_{j}^{L}}}{\sum_{k=1}^{m}{e^{z_{k}^{L}}}}
\end{equation}
Es gilt:
\begin{equation}
	\sum_{j=1}^{m}{a_{j}^{L}}=1
\end{equation}

\subsection{Gradientenverfahren}
Das neuronale Netzwerk lernt indem es die Kostenfunktion $C$ minimiert. Dazu müssen die Gewichtungen $w_{jk}^{l}$ sowie die Biase $b_{j}^{l}$ angepasst werden. Erreicht wird dies durch die Anwendung des Gradientenverfahrens.
Bevor dieses Verfahren im Kontext der neuronalen Netze beschrieben wird, erfolgt die Erläuterung an einem einfacheren Beispiel.

\begin{figure}[tb]
  \centering
  \subfloat[Ausgangssituation]{\includegraphics[width=0.49\textwidth]{recognize/nn/gradBegin}}
  \hfill
	\subfloat[Zustand nach 200 Iterationen]{\includegraphics[width=0.49\textwidth]{recognize/nn/gradFinish}}
  \caption{Gradientenverfahren am Beispiel der Funktion $x^2+y^2$}
  \label{fig:graddesc}
\end{figure}
\subsubsection{Beispielhafte Anwendung des Gradientenverfahren}
Betrachtet wird die Funktion $f:
\realnumbers^{2} \rightarrow \realnumbers$, die definiert ist durch:
\begin{equation}
	f(x,y)=x^2+y^2
\end{equation}
Ziel ist, Werte für $x$ und $y$ zu finden, sodass $f(x,y)$ minimal ist.
Begonnen wird indem $x$ und $y$ zufällig initialisiert werden.
Beispielsweise sei $x=-4$ und $y=-3$. Die Situation ist im linken Bild der Abbildung
~\ref{fig:graddesc} dargestellt. Der rote Ausgangspunkt hat die Koordinaten
$(-4,-3, 25)$. Wie bereits aus Abschnitt ~\ref{sec:grads} bekannt ist, gibt der
Gradient einer Funktion die Richtung des steilsten Anstiegs an. Die Idee hinter
dem Gradientenverfahren ist den Ausgangspunkt in kleinen Schritten in die
entgegengesetzte Richtung des Gradienten zu verschieben. Algorithmus ~\ref{alg:gradexample} zeigt die Vorgehensweise. Wie im rechten Bild der
Abbildung ~\ref{fig:graddesc} erkennbar ist, nähert sich so der rote Punkt der
Position $(0, 0)$ an, bei der $f$ minimal ist.

\begin{algorithm}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \textcolor{cadmiumgreen}{\textbf{GradientDescent}}$(f, p, \eta, n)$\;
    \Indp\Input{$f: \realnumbers^{n} \rightarrow \realnumbers$: stetig differenzierbare Funktion, $p \in \realnumbers^{n}$: Ausgangspunkt, $\eta$: Schrittweite mit $\eta>0$, $n$: durchzuführende Iterationen}
    \Output{Position von $p$ nach $n$ Iterationen des Gradientenverfahrens}
		\BlankLine
    \For{$i \gets 1$ \KwTo $n$}
		{
			$p \gets p - \eta \nabla f(p)$
		}
    \textbf{return} $p$\;
    \caption{Durchführung des Gradientenverfahrens}
		\label{alg:gradexample}
\end{algorithm}

\subsubsection{Optimierung der Kostenfunktion}
Zum Training des neuronalen Netzwerks wird das Gradientenverfahren auf die Kostenfunktion $C$ angewandt um Gewichtungen $w_{jk}^{l}$ sowie Biase $b_{j}^{l}$ zu finden mit denen $C$ minimal ist. Das Verfahren beginnt, indem alle $w_{jk}^{l}$ und $b_{j}^{l}$ initialisiert werden. Der Ausgangswert aller Biase kann im Gegensatz zu den Gewichtungen auf $0$ gesetzt werden \cite[14]{bengio2012practical}. Eine Übersicht der Methoden zur Initialisierung der Gewichtungen wurde in \cite{cs231nPartTwo} veröffentlicht.

In jeder Iteration des Gradientenverfahrens werden folgende Operationen für alle Gewichte sowie Biase ausgeführt.

\begin{equation}
	w_{jk}^{l} \gets w_{jk}^{l} - \eta \frac{\partial C}{\partial w_{jk}^{l}}
\end{equation}

\begin{equation}
	b_{j}^{l} \gets b_{j}^{l} - \eta \frac{\partial C}{\partial b_{j}^{l}}
\end{equation}

\subsection{Stochastisches Gradientenverfahren}
Ein in der Praxis auftretendes Problem ist, dass die Kostenfunktion $C$ von allen Paaren $(x,y)\in T$ der Trainingsdaten abhängt. Um in jeder Iteration des Gradientenverfahren $\frac{\partial C}{\partial w_{jk}^{l}}$ und $\frac{\partial C}{\partial b_{j}^{l}}$ zu bestimmen muss für alle Eingaben $x$ der Output $a^{L}$ des neuronalen Netzwerks berechnet werden. Somit hat jede Iteration des Gradientenverfahrens eine hohe Laufzeit, wodurch das Training verlangsamt wird.

Zur Lösung des Problems wird zuerst die Kostenfunktion $C$ betrachtet:
\begin{equation}
	C=-\frac{1}{|T|}\sum_{(x,y)\in T}{\sum_{i=1}^{m}{(y_{i}\text{ ln}(a_{i}^{L})+(1-y_{i})\text{ ln}(1-a_{i}^{L})))}}
\end{equation}

Diese lässt sich wie folgt in zwei Teile zerlegen:
\begin{equation}
	C_{(x,y)}=\sum_{i=1}^{m}{(y_{i}\text{ ln}(a_{i}^{L})+(1-y_{i})\text{ ln}(1-a_{i}^{L})))}
\end{equation}

\begin{equation}
	C=-\frac{1}{|T|}\sum_{(x,y)\in T}{C_{(x,y)}}
\end{equation}

Wie erkennbar ist, handelt es sich bei $C$ um den Durchschnitt der Kosten der einzelnen Trainingsdaten $(x,y)\in T$. Für die partiellen Ableitungen gilt:

\begin{equation}
	\frac{\partial C}{\partial w_{jk}^{l}} = -\frac{1}{|T|}\sum_{(x,y)\in T}{\frac{\partial C_{(x,y)}}{\partial w_{jk}^{l}}}
\end{equation}

\begin{equation}
	\frac{\partial C}{\partial b_{j}^{l}} = -\frac{1}{|T|}\sum_{(x,y)\in T}{\frac{\partial C_{(x,y)}}{\partial b_{j}^{l}}}
\end{equation}


Das Problem, dass $C$ von allen $(x,y)\in T$ abhängt, wird durch die Verwendung des stochastischen Gradientenverfahren gelöst. Die Idee ist eine zufällig gewählte kleine Teilmenge der Trainingsdaten mit $m$ Elementen zu bestimmen, also $\hat{T}=\{(x_{1},y_{1}),...,(x_{m},y_{m})\}\subset T$ und mit dieser $\frac{\partial C}{\partial w_{jk}^{l}}$ und $\frac{\partial C}{\partial b_{j}^{l}}$ anzunähern, also:

\begin{equation}
	\frac{\partial C}{\partial w_{jk}^{l}} = -\frac{1}{|T|}\sum_{(x,y)\in T}{\frac{\partial C_{(x,y)}}{\partial w_{jk}^{l}}} \approx -\frac{1}{|\hat{T}|}\sum_{(x,y)\in \hat{T}}{\frac{\partial C_{(x,y)}}{\partial w_{jk}^{l}}}
\end{equation}

\begin{equation}
	\frac{\partial C}{\partial b_{j}^{l}} = -\frac{1}{|T|}\sum_{(x,y)\in T}{\frac{\partial C_{(x,y)}}{\partial b_{j}^{l}}} \approx -\frac{1}{|\hat{T}|}\sum_{(x,y)\in \hat{T}}{\frac{\partial C_{(x,y)}}{\partial b_{j}^{l}}}
\end{equation}

Da $\hat{T}$ weniger Elemente als $T$ hat, benötigen die einzelnen Iterationen des Gradientenverfahrens weniger Laufzeit.
$\hat{T}$ wird häufig Mini Batch genannt und die Zahl $m$ Batch Size. Nachdem mithilfe der partiellen Ableitungen alle Gewichte und Biase angepasst wurden, wird eine andere zufällige Teilmenge gewählt. Sobald alle Elemente von $T$ verwendet wurden spricht man vom Ende einer Epoche.


\section{Convolutional Neuronal Networks}
Convolutional Neuronal Networks konnten im Bereich der Bilderkennung eine State of the Art Performance erreichen \cite{krizhevsky2012imagenet}. Aus diesem Grund wurde in dieser Arbeit ein CNN zur Erkennung von Text verwendet.

CNNs sind den neuronalen Netzen sehr ähnlich. Sie werden mithilfe des stochastischen Gradientenverfahrens trainiert, verfügen über Gewichte, Biase und Aktivierungsfunktionen. Zusätzlich wird am Ende eines CNN ein ''normales'' neuronales Netz angehängt. Der größte Unterschied ist, dass CNNs Operationen nicht auf Vektoren, sondern Tensoren ausführen. Dadurch kann ein Graustufenbild direkt als Eingabe eines CNN verwendet werden. Weiterhin verfügen CNNs im Gegensatz zu neuronalen Netzen über unterschiedliche Arten von Schichten. Die folgende kurze Einführung basiert auf \cite{cs231ncnn} und \cite{cnnintro}.

\subsection{Tensoren}
Tensoren lassen sich intuitiv wie folgt beschreiben. Betrachtet wird ein Vektor $v\in \realnumbers^{H}$. Wenn nun $W$ Vektoren nebeneinander platziert werden entsteht eine Matrix $A\in \realnumbers^{H \times W}$. Ein Vektor wird Tensor erster Ordnung genannt, eine Matrix Tensor zweiter Ordnung. Das Prinzip lässt sich beliebig wiederholen. Ein RGB-Bild mit $100$ Zeilen, $100$ Spalten und drei Kanälen kann beispielsweise als Tensor dritter Ordnung $x \in \naturalnumbers^{100 \times 100 \times 3}$ aufgefasst werden. Jedes Element von $x$ kann durch ein Tupel $(i,j,d)$ mit $0 \le i < 100, 0 \le j < 100, 0 \le d < 3$ indiziert werden.

\subsection{Schichten}
Ein CNN erhält als Eingabe einen Tensor $x \in \realnumbers^{H \times W \times D}$. Danach folgen Convolutional Schichten und Max Pool Schichten. Am Ende des CNN wird eine Flatten Operation ausgeführt, die den Ausgabetensor des CNN in einen Vektor konvertiert. Dieser dient als Eingabe des angehängten neuronalen Netzes.

\subsubsection{Convolutional Schicht}
Ähnlich wie bei neuronalen Netzen wird bei der Convolutional Schicht eine Kombination der Eingabe $x\in \realnumbers^{H \times W \times D}$ mit Gewichtungen $w$ und einem Bias $b$ durchgeführt. Die Idee ist die Gewichtungen in $F$ unterschiedlichen Kernel $k \in \realnumbers^{K \times K \times D}$ zu speichern. Somit gilt $w\in \realnumbers^{K \times K \times D \times F}$. Der Bias $b\in \realnumbers$ ist weiterhin ein Skalar.

Die gewichtete Eingabe der Convolutional Schicht wird berechnet, indem die Eingabe $x$ mit den $F$ unterschiedlichen Kernel gefaltet wird wodurch ein Tensor $z \in \realnumbers^{H' \times W' \times F}$ entsteht. Vor der Faltung besteht die Option einen Rand von $P$ Pixeln, welche alle den Wert $0$ haben zu $x$ hinzuzufügen. Anschließend wird zu jedem Element von $z$ der Bias $b$ hinzugefügt. Die Höhe $H'$ und Breite $W'$ der gewichteten Eingabe lassen sich berechnen als:
\begin{equation}
	H'=W'= (W-K+2P)+1
\end{equation}
Die Ausgabe $a \in \realnumbers^{H' \times W' \times F}$ der Schicht wird wie bei neuronalen Netzen durch das Anwenden der Aktivierungsfunktion $f: \realnumbers \rightarrow \realnumbers$ auf alle Elemente der gewichteten Eingabe $z$ berechnet.

\subsubsection{Max Pool Schicht}
Die Max Pool Schicht wird verwendet, um die Höhe $H$ und Breite $W$ einer Eingabe $x \in \realnumbers^{H \times W \times D}$ zu verringern. Sie verfügt über keine Gewichtungen und Biase. Das Pooling arbeitet die $D$ Kanäle der Eingabe unabhängig voneinander ab. Dabei wird ein Fenster mit Breite und Höhe von $K$ Pixeln über die Eingabe geschoben und das Maximum der Pixelwerte innerhalb des Fensters an der entsprechenden Stelle der Ausgabe $a\in \realnumbers^{H'\times W' \times D}$ gespeichert. Anschließend wird das Fenster um $S$ Pixel nach rechts verschoben. Sobald der rechte Rand erreicht wurde, wird das Fenster um $S$ Pixel nach unten geschoben und vom linken Rand erneut begonnen. So wird fortgefahren, bis die rechte untere Ecke des Bildes erreicht wurde. Die Höhe $H'$ und Breite $W'$ der Ausgabe lassen sich berechnen als:
\begin{equation}
	H'=W'= \frac{W-K}{S} + 1
\end{equation}

\section{Synthetische Trainingsdaten}
\label{sec:train}
\begin{figure}[t!]
	\centerfloat
  \includegraphics[width=0.75\textwidth]{recognize/synth.pdf}
	\caption{Erstellung synthetischer Trainingsdaten}
	\label{fig:synth}
\end{figure}
\label{chp:evaluation}
\begin{figure}[t!]
  \centering
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/1}}
  \hfill
	\subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/2}}
  \hfill
	\subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/3}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/4}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/5}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/6}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/7}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/8}}
  \hfill
  \subfloat{\includegraphics[width=0.1\textwidth]{recognize/samples/9}}
  \caption{Beispiele für generierte Trainingsdaten}
  \label{fig:synthOut}
\end{figure}
Da die Genauigkeit von Convolutional Neuronal Networks mithilfe von
Trainingsdaten gesteigert wird, ist ein Datensatz, der aus vielen Trainingsdaten
besteht, notwendig. In der Praxis stellt vor allem das Labeling von Bildern ein
Problem dar, da zeitaufwendiges menschliches Eingreifen notwendig ist. Eine
Alternative ist die synthetische Generierung von Trainingsdaten, bei der sowohl
die Bilder als auch die zugehörigen Labels automatisch erstellt werden.
Abbildung ~\ref{fig:synthOut} zeigt einige der Textzeichen, die von der im
Folgenden beschriebenen Methode erzeugt wurden. Die Vorgehensweise orientiert
sich an \cite{jaderberg2016reading}. Eine abstrakte Übersicht der Methode zur
Erzeugung von Trainingsdaten ist in Abbildung ~\ref{fig:synth} dargestellt.

\subsubsection{Generierung von Schriftzeichen}
Da die Schriftart von Text in natürlicher Umgebung stark variieren kann, werden
zur Erzeugung der Textzeichen 873 unterschiedliche Schriftarten aus dem
Repository \footnote{https://github.com/google/fonts} von Google Fonts
verwendet.

Um in natürlichen Szenarien typisch auftretende Farben zu erhalten, wird der k-means Algorithmus mit $k=3$ auf den Street View Text (SVT) Datensatz
\cite{svt} angewandt. So wird jedes Bild des Datensatzes auf 3 Farben reduziert, die in einer Datei abgespeichert werden.

Die Generierung der Schriftzeichen beginnt, indem zufällig eine der 873
Schriftarten ausgewählt wird. Ebenfalls werden aus der zuvor beschriebenen Datei
zufällig zwei Farben gewählt. Eine für das Textzeichen, eine andere für den
Hintergrund. Anschließend wird ein Bild $I:D \rightarrow V$ mit einer Höhe sowie
Breite von $32$ Pixel erzeugt. Darauf folgend wird das zu generierende
Textzeichen in der gewählten Schriftart mittig in $I$ platziert. Die
Schriftgröße ist ebenfalls zufällig. Sie wird durch einen normalverteilter
Zufallsgenerator mit Erwartungswert $\mu=40$ und Standardabweichung $\sigma=4$
bestimmt.

\subsubsection{Geometrische Transformationen}
Nachdem das Schriftzeichen generiert wurde werden die im folgenden beschriebenen
geometrische Transformationen mit einer Wahrscheinlichkeit von jeweils $40$\%
auf $I$ angewandt.

\begin{enumerate}
	\item Rotation von $I$ um $\theta$ Grad wobei $\theta$ zufällig aus dem
	Bereich $[-10, 10]$ gewählt wird.
	\item Verschiebung des Textzeichen um $x$ Pixel in der x-Richtung und um $y$ Pixel in der
	y-Richtung. $x$ und $y$ sind zufällig aus dem Bereich $[-3,3]$ ausgewählt.
	\item Anwenden einer zufälligen perspektivischen Transformation auf $I$.
\end{enumerate}

\subsubsection{Blending}
Aus einem der Bilder des Street View Text Datensatz wird eine zufällig bestimmte
Region mit einer Höhe und Breite von jeweils $32$ Pixel ausgeschnitten. Das so
entstandene Bild $J:D \rightarrow V$ wird mit dem generierten Bild $I$ vermischt,
wodurch ein Bild $K:D \rightarrow V$ entsteht. $K$ ist dabei gegeben durch:
\begin{equation}
	K(x,y)=\text{round}(\alpha I(x,y) + (1-\alpha) J(x,y))
\end{equation}

$\alpha$ wird zufällig aus dem Bereich $[0.7,1.0]$ gewählt. Die Funktion
$\textit{round}$ rundet auf die nächste ganze Zahl.

\subsubsection{Rauschen}
Abschließend werden mit einer Wahrscheinlichkeit von jeweils $20$\% folgende
Operationen durchgeführt:

\begin{enumerate}
	\item Glättung von $K$ mit einem Gaussian Kernel $H_{G,1}$.
	\item Hinzufügen von Salz und Pfeffer Rauschen zu $K$.
	\item Hinzufügen von additivem weißen gaußschen Rauschen zu $K$.
\end{enumerate}

\begin{figure}[tb]
	\centerfloat
  \includegraphics[width=1.3\textwidth]{recognize/cnn.pdf}
	\caption{Architektur des entwickelten CNN}
	\label{fig:cnnarch}
\end{figure}
\section{Entwickeltes CNN}
\label{sec:devcnn}
Die Festlegung der Architektur als auch das Training des entwickelten CNN wurden mithilfe der quelloffenen Python Library Keras \cite{chollet2015keras} durchgeführt. Diese Bibliothek ermöglicht eine einfache Verwendung von CNNs, wobei die eigentlichen Berechnungen durch die Library Tensorflow \cite{tensorflow2015-whitepaper} durchgeführt werden.

Bei der Entwicklung des CNN wurde sich an dem in \cite{simonyan2014very} vorgestellten Modell \textit{B} orientiert. Es wurde kein Transfer learning \footnote{Transfer learning bezeichnet die Verwendung eines anderen bereits vortrainierten Netzwerks.} verwendet. Im Gegensatz zu \cite{simonyan2014very} werden weniger Convolution- und Max-Pool-Operationen durchgeführt. Grund hierfür ist, dass die gewählte Anzahl an Schichten ausreichend war, um eine hohe Genauigkeit zu erreichen. \footnote{Eine exakte Analyse der Genauigkeit wird in Abschnitt ~\ref{sec:textrecogaccuracy} durchgeführt.}

\subsection{Architektur}
Wie in ~\ref{fig:cnnarch} erkennbar ist verfügt das CNN über $L=12$ Schichten. Die Eingabe $x$ ist ein Graustufenbild mit einer Höhe und Breite von $32$ Pixel also $x\in \realnumbers^{32\times 32 \times 1}$. Bei der Ausgabe $a^{L}$ handelt es sich um einen Vektor mit $62$ Komponenten. Der Index $i$ des vom CNN erkannten Zeichen $c_{i}$ mit $c=(A,...,Z$,\allowbreak$a,...,z$,$0,...,9)$ ist gegeben als:

\begin{equation}
	i = \argmax_{j \in \{1,...,62\}} a^{L}_{j}
\end{equation}

Alle der Eingabe folgenden Convolutional Schichten verfügen über Kernel der Größe $K=5$. Diese werden durch eine Faltung angewandt. Die Anzahl der Kernel $F$ pro Convolutional Schicht wird nach und nach von $64$ auf $256$ erhöht wie in Abbildung ~\ref{fig:cnnarch} erkennbar ist. Alle Max Pool Schichten des Netzwerks verwenden die Größe $K=2$ und eine Stride $S=2$.
Das Padding $P$ aller Convolutional Schichten ist so gesetzt, dass die Höhe und Breite der Ausgabe der Höhe und Breite der Eingabe entsprechen. Der Output der letzten Convolutional Schicht wird in einen Vektor mit $16384$ Komponenten konvertiert der die Eingabe eines neuronalen Netzwerks darstellt. Dieses besteht aus drei Schichten wobei die Erste und Zweite über $2048$ Neuronen verfügen. Die dritte Schicht ist die Ausgabeschicht mit $62$ Neuronen.

Die Aktivierungsfunktionen aller Convolutional Schichten ist ReLU. Die ersten beiden Schichten des neuronalen Netzes (Layer 10 und 11) verwenden ebenfalls ReLU. Bei der Ausgabeschicht wird wie in Abschnitt ~\ref{sec:costfunction} beschrieben die Softmax Funktion verwendet.

\begin{figure}[tbh]
\begin{tikzpicture}
  \begin{axis}[xtick=data, ytick={0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0}, height=9cm, grid=both, xlabel=Epoche, ylabel=Accuracy]
  \addplot coordinates {
    ( 0, 0)
    ( 1, 0.192 )
    ( 2, 0.540 )
    ( 3, 0.649 )
    ( 4, 0.700 )
    ( 5, 0.730 )
    ( 6, 0.754 )
    ( 7, 0.772 )
    ( 8, 0.786)
    ( 9, 0.801 )
    ( 10, 0.811 )
    ( 11, 0.823 )
    ( 12, 0.834 )
    ( 13, 0.841 )
    ( 14, 0.850 )
    ( 15, 0.856 )
  };
  \end{axis}
\end{tikzpicture}
  \centering
  \caption{Entwicklung der CNN Accuracy über die verschiedenen Epochen}
	\label{fig:cnnaccdiag}
\end{figure}
\subsection{Training}
Als Kostenfunktion $C$ wurde die Kreuzentropie festgelegt. Bei den Trainingsdaten $T$ handelt es sich um $97216$ synthetisch generierte Bilder mit entsprechenden Labels.

Als Optimierungsverfahren wurde das stochastische Gradientenverfahren mit einer Schrittweite von $\eta=0.0001$ und einer Batch Size von $128$ verwendet. $\eta$ wird adaptiv durch rmsprop \cite{rmsprop} mit einem Decay von $\frac{1}{1000000}$ angepasst. Die Entwicklung der Genauigkeit des CNN über die $15$ durchgeführten Epochen ist in Abbildung ~\ref{fig:cnnaccdiag} dargestellt.

Zur Vermeidung von Overfitting \footnote{Overfitting bezeichnet den Zustand, dass ein CNN eine hohe Genauigkeit unter Verwendung der Trainingsdaten erzielt, aber eine deutlich geringere Genauigkeit bei Daten, die nicht Teil der Trainingsdaten sind. In diesem Fall wurde keine generelle Lösung des Problems gefunden.} wurde in Layer $10$ und $11$ ein Dropout \cite{srivastava2014dropout} von $50$\% eingefügt.
