\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{4}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{4}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Zielsetzung}{5}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}System\IeC {\"u}bersicht und Definitionen}{7}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}System\IeC {\"u}bersicht}{7}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Digitale Bilder}{8}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Lokalisierung von Text}{10}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Connected Components}{10}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Nachbarschaft von Pixeln (Pixel Connectivity)}{11}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Definition einer Connected Component}{11}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Attribute der Connected Components}{12}{subsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Fl\IeC {\"a}che}{12}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Bounding Box}{12}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Seitenverh\IeC {\"a}ltnis}{13}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Extent}{13}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Solidity}{13}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Maximally Stable Extremal Regions}{14}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Bin\IeC {\"a}res Thresholding}{14}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Definitionen}{15}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Implementierung}{16}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Suche der Extremal Regions}{16}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Auswahl der Maximally Stable Extremal Regions}{16}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Gradienten}{18}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Intuition}{19}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Definition}{19}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Berechnung des Gradienten}{19}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Kernel}{20}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Faltung und Korrelation}{21}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Sobel}{21}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Canny Edge Detection}{23}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Vorbearbeitung}{23}{subsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Lokalisierung der Kanten}{25}{subsection.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Hysteresis Thresholding}{26}{subsection.3.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Edge Enhanced MSER}{28}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Durchf\IeC {\"u}hrung des Edge Enhancing}{28}{subsection.3.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Stroke Width Transform}{29}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.1}Distance Transform}{31}{subsection.3.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.2}Anwendung der Stroke Width Transform}{32}{subsection.3.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.7}Robuste Textlokalisierung}{34}{section.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameter}{35}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Praktische Umsetzung}{36}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Erkennung von Text}{37}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Neuronale Netze}{37}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Neuronen}{37}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Bildung von neuronalen Netzwerken}{38}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Aktivierungsfunktionen}{39}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Trainingsdaten}{40}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Kostenfunktion}{40}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.6}Gradientenverfahren}{41}{subsection.4.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Beispielhafte Anwendung des Gradientenverfahren}{42}{section*.38}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Optimierung der Kostenfunktion}{42}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.7}Stochastisches Gradientenverfahren}{43}{subsection.4.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Convolutional Neuronal Networks}{44}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Tensoren}{44}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Schichten}{44}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Convolutional Schicht}{45}{section*.41}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Max Pool Schicht}{45}{section*.42}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Synthetische Trainingsdaten}{45}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Generierung von Schriftzeichen}{46}{section*.45}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Geometrische Transformationen}{47}{section*.46}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Blending}{47}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Rauschen}{47}{section*.48}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Entwickeltes CNN}{48}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Architektur}{48}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Training}{49}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Merging und Fehlerkorrektur}{51}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Merging}{51}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Fehlerkorrektur}{52}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Levenshtein Distanz}{53}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Korrektur}{53}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Beispiel}{54}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Evaluation}{55}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Textlokalisierung}{56}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.1}Precision und Recall}{56}{subsection.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Formale Definition}{56}{section*.56}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Ergebnis}{57}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.2}Vergleich}{57}{subsection.6.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Texterkennung}{58}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.1}Confusion Matrix}{58}{subsection.6.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Komplettes System}{60}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Laufzeit}{60}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Schlussbemerkung und Ausblick}{62}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Ausblick}{62}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Anhang}{70}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Stroke Width Transform Strichbreite}{70}{section.8.1}
